package com;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class hmac {

	private static Logger log = Logger.getLogger(hmac.class.getName());
	
	public static String getHmac256Digest(String message, String secretKey) {
		try {
			return getLowercaseHexDigest(
					getMacBytes(message, secretKey, "HmacSHA256"));
		} catch (InvalidKeyException e) {
			log.log(Level.SEVERE, e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			log.log(Level.SEVERE, e.getMessage());
		} catch (IllegalStateException e) {
			log.log(Level.SEVERE, e.getMessage());
		} catch (UnsupportedEncodingException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
		return "";
	}
	
	public static byte[] getMacBytes(String message, String secretKey, String algorithm) throws NoSuchAlgorithmException, IllegalStateException, UnsupportedEncodingException, InvalidKeyException {
		SecretKeySpec key = new SecretKeySpec(secretKey.getBytes("UTF-8"), algorithm);
		Mac mac = Mac.getInstance(algorithm);
		mac.init(key);
		return mac.doFinal(message.getBytes("ASCII"));
	}
	
	public static String getLowercaseHexDigest(byte[] bytes) {
		StringBuffer hash = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				hash.append('0');
			}
			hash.append(hex);
		}
		return hash.toString();
	}
}
