output application/json
var status = vars."sys-fb-post-payload".status
var message = p(vars.finalMessageKey default "")
var finalStatus = if(status==null)
	1
else
	status
	
var finalMessage = if(finalStatus ~= "0")
	p('response.success')
else if(not (message == null))
	message
else
	p('response.failure')

---
{
	"status": finalStatus,
	"message": finalMessage,
	"sms_message": vars.sms_message
}