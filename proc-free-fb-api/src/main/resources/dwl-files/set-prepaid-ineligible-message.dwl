%dw 2.0
output application/java
var finalMessage = if(vars.bin ~= "1")
	p("text.Opt-in.1.ineligible")
else if(vars.bin ~= "2")
	p("text.Opt-in.2.ineligible")
else
	p("text.error")
---
finalMessage