%dw 2.0
output application/java
var isBonus = vars.action=="Bonus"
var isOptOut = vars.action == "Opt-out"
var isPostpaid = vars.bin ~= 3
var isStateNull = vars.state == null
var isNotError = vars.isError == null
var expiryDate=if(vars.expiry==null)
	""
else if(isPostpaid)
	((vars.param_expiry as DateTime as Date {format: 'MM/dd/yyyy'}) as String default "")
else if(isBonus and (not isPostpaid) and (not isStateNull and vars.state ~= "0"))
	((vars.expiry as DateTime) as String {format: 'MM/dd/yyyy, hh:mm'} default "")
else
	((vars.expiry as DateTime as Date {format: 'MM/dd/yyyy'}) as String default "")
var initialMessage=if (isNotError and isOptOut and isStateNull)
	"text.invalid_number"
else if (isNotError)
	"text." ++ (vars.action default "") ++ "." ++ (vars.bin as String) ++ "." ++ (vars.state default "null" as String)
else
	"text.error"
	
var subMessage = p(initialMessage)
var finalMessage=if(subMessage==null)
	p('text.error')
else if((subMessage contains "{1}") and (not (expiryDate =="")))
	subMessage replace "{1}" with expiryDate
else 
	subMessage
---
finalMessage