%dw 2.0
output application/json
import java!com::DateUtils
---
{
	"msisdn": vars.msisdn,
	"timestamp": vars.timestamp,
	"provision": {
		"fb_product_id": "free",
		"expiration": vars.expiry
	}
}
