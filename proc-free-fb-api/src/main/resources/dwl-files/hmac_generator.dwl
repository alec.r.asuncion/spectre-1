%dw 2.0
output application/java
import java!com::hmac
var valueForMethod = vars.timestamp as String ++ p('carrier_id')
---
hmac::getHmac256Digest(valueForMethod, p('shared-key'))