output application/java
var isBonus = vars.action=="Bonus"
var isOptIn = vars.action == "Opt-in"
var isOptOut = vars.action == "Opt-out"
var isPostpaid = vars.bin ~= 3
var isStateNullOrZero = vars.state ~= "0" or vars.state ~= "null"

var isEligible= if(isBonus and isPostpaid)
	false as Boolean
else if(((isBonus or isOptIn) and (isStateNullOrZero)) or
	(isOptOut and (vars.state ~= "1" or vars.state ~= null)))
	true as Boolean
else 
	false as Boolean
---
isEligible